# Simple nodes library for path traversing only
# May not a better one :-(
class Node:

    parent = None;
    childrens = []

    def __init__(self, value):
        self.value = value;

    def add_children(self, node):
        node.parent = self;
        self.childrens.append(node);
        return node;

    def has_parent(self):
        return self.parent != None;
