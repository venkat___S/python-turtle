import turtle

t = turtle.Turtle()
# t.tracer(False)
t.speed(0)

for i in range(36*2):
    t.penup()
    t.goto(0,0)
    t.pendown()
    t.setheading(0)
    t.left(i*5)
    for j in [(4*25,45+90),(4*25,45+45),(3*25,45+180)]:
		t.forward(j[0])
		t.right(j[1])
		
turtle.Screen().exitonclick()