### 01 -> Slide Puzzle (8 - Puzzle)

Solving the 3x3 grid puzzle using the Breath First Search Algorithm

### 02 -> Sudoku

Solving the Sudoku puzzle using the backtracking Algorithm

### 03 -> KnightsTour

Solving the KnightsTour Chess board challenge using the backtracking Algorithm
