import random, sys
sys.path.append('../')
from Libraries.Node import *

# Forget about the index, all the numbers are in position
# Starting from 1, not zero
class SlidePuzzle:


	# Constructor to initilize the puzzle with the given input
	# puzzle -> List of the puzzle state to be checked later
	# solution -> Stores the solution path in an array as Node Instance
	# Node Values stored as -> (previous_move, current_state)
	# expected_output -> declaring the expected output
	# for easy comparision (check) at easy iteration
	# state -> Used for saving the state
	# Using Dict instead of list to avoid time consumption during index search
	# It improves the speed :-)
	def __init__(self, default_input):
		self.puzzle = [Node(("",default_input))];
		self.solution = [];
		self.expected_output = "123456780";
		self.state = {};


	# To find the position of the zero
	def get_position_of_zero(self,puzzle):
		return puzzle.index("0")+1

	# Tracing the path from the solution node to the Parent Node
	def path_trace(self, node):
		while node.has_parent():
			self.solution.append(node)
			node = node.parent
		self.solution.append(node)
		self.solution.reverse();

	# TODO: Need to generate solvable input, All the permunations of this puzzle won't have solution
	# Generates the puzzle by placing random numbers
	# Have to write the function :-(
	def generate_puzzle(self, default_input):
		return;

	# To list the possible move we can swap with the "0"
	# output = [(Starting letter of the possible move, Posiible positions),(),()]
	# Position will start from 1 (Not 0)
	# input = position -> Position of the zero
	def possible_moves(self, position):
		output = [];
		if position - 1 > 0 and ((position - 1)%3) != 0:
			move = ("L",position - 1)
			output.append(move)
		if position + 3 <= 9:
			move = ("D",position + 3)
			output.append(move)
		if position + 1 <= 9 and (position % 3) != 0:
			move = ("R",position + 1)
			output.append(move)
		if position - 3 > 0:
			move = ("U",position - 3)
			output.append(move)
		return output;

	# Solving the puzzle with the BFS -> Breath First Search
	# puzzles = ["String of numbers"]
	# String of numbers = "123456789"
	def solve(self, puzzles):
		swapped_puzzles = []
		for puzzle in puzzles:
			zero_position = self.get_position_of_zero(puzzle.value[1])
			possible_moves = self.possible_moves(zero_position)
			for move in possible_moves:
				swapped_puzzle = self.swap(puzzle.value[1], zero_position, move[1])
				if self.check(swapped_puzzle):
					solved_node = Node((move[0], swapped_puzzle));
					puzzle.add_children(solved_node);
					self.path_trace(solved_node)
					return True
				try:
					self.state[swapped_puzzle]
				except:
					self.state[swapped_puzzle] = True;
					swapped_node = Node((move[0], swapped_puzzle))
					puzzle.add_children(swapped_node);
					swapped_puzzles.append(swapped_node);
		if len(swapped_puzzles) != 0:
			return self.solve(swapped_puzzles);


	# Just for pretty printing the puzzle state
	# Will be easy for viewing :-)
	def pretty_print(self, puzzle):
		print "-"*13
		print "|",puzzle[0],"|",puzzle[1],"|",puzzle[2],"|"
		print "|",puzzle[3],"|",puzzle[4],"|",puzzle[5],"|"
		print "|",puzzle[6],"|",puzzle[7],"|",puzzle[8],"|"
		print "-"*13

	# Prints the array of solutions
	def print_solution(self):
		for solution in self.solution:
			print solution.value[0]
			puzzle.pretty_print(solution.value[1])

	# Checks the puzzle and expected output
	# Just have if condition to check the puzzle state
	def check(self, puzzle):
		if puzzle == self.expected_output:
			return True
		return False

	# Simple swap function to swap to numbers, by their positions
	# from_char, to_position is the actual position of the number (Again not index)
	# All index related problems are handled inside the class functions
	def swap(self, puzzle, from_position, to_position):
		from_char = puzzle[from_position - 1];
  		to_char = puzzle[to_position - 1];
  		puzzle = puzzle.replace(from_char, "*");
  		puzzle = puzzle.replace(to_char, from_char);
  		puzzle = puzzle.replace("*", to_char);
  		return puzzle;

puzzle = SlidePuzzle("547123860");
puzzle.solve(puzzle.puzzle);
puzzle.print_solution()
