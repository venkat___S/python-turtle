
# TODO: Need to have grid size changeable, Now it is fixed to 9x9

class Sudoku:

	grid_cache = {
	1: [1,2,3,10,11,12,19,20,21],
	2: [4,5,6,13,14,15,22,23,24],
	3: [7,8,9,16,17,18,25,26,27],
	4: [28,29,30,37,38,39,46,47,48],
	5: [31,32,33,40,41,42,49,50,51],
	6: [34,35,36,43,44,45,52,53,54],
	7: [55,56,57,64,65,66,73,74,75],
	8: [58,59,60,67,68,69,76,77,78],
	9: [61,62,63,70,71,72,79,80,81]
	}

	# initilize the grid as a json
	# Mark the 0 values in to_be_filled_array
	def __init__(self, input):
		self.to_be_filled_array = []
		self.input = {};
		count = 1;
		for i in input:
			for j in i:
				self.input[count] = int(j)
				if int(j) == 0:
					self.to_be_filled_array.append(count)
				count = count +1;

	# to check whether the value can be placed in the position
	def check_value(self, value, position):
		return self.check_row(value, position) and self.check_column(value, position) and self.check_grid(value, position);

	# Check the corresponding row
	def check_row(self, value, position):
		for i in range(9):
			if self.input[position+i] == value:
				return False;
			if (position+i)%9 == 0:
				break;
		for i in range(1,9):
			if (position-i)%9 == 0:
				break;
			if self.input[position-i] == value:
				return False;
		return True;

	# Check the corresponding column
	def check_column(self, value, position):
		for i in range(9):
			if position+(i*9) > 81:
				break;
			if self.input[position+(i*9)] == value:
				return False;
		for i in range(1,9):
			if position-(i*9) <= 0:
				break;
			if self.input[position-(i*9)] == value:
				return False;
		return True;

	# TODO: Have to generate the Grid on the flow (No Hard Coding)
	# Checks the corresponding grid
	def check_grid(self, value, position):
		for i in range(1,10):
			grid = self.grid_cache[i];
			try:
				grid.index(position)
				for j in range(9):
					if self.input[grid[j]] == value:
						return False;
				break;
			except ValueError:
				pass;
		return True

	# This is the only method to place the values in the puzzles
	# Never directly place the values using self.input
	def place_value(self, value, position):
		self.input[position] = value;
		return True

	# Prints the sudoku puzzle in the user readable format
	def pretty_print(self):
		print "+-" + ("-"*10) + "+" + ("-"*11) + "+" + ("-"*11) + "+";
		c_line = "";
		for i in range(1,82):
			c_line =  c_line + "|"
			inp = " " + str(self.input[i]) + " ";
			if i in self.to_be_filled_array:
				inp = "(" + str(self.input[i]) + ")"
			c_line = c_line + inp;
			if i % 27 == 0:
				c_line = c_line + "|\n+-" + ("-"*10) + "+" + ("-"*11) + "+" + ("-"*11) + "+\n"
			elif i % 9 == 0:
				c_line = c_line + "|\n"
			elif i % 3 == 0:
				c_line = c_line
		print c_line;

	# Checks whether the puzzle is fully solved
	# No need to check the entire loop
	# Just the last position in the to_be_filled_array is not 0
	def is_solved(self):
		if self.input[self.to_be_filled_array[-1]] == 0:
			return False
		return True

	# Solves the sudoku puzzle using back tracking algorithm
	def solve(self,i=0):
		while i < len(self.to_be_filled_array):
			position = self.to_be_filled_array[i]
			placed, start = False, 0
			if self.input[position] != 0:
				start = self.input[position]
			for value in range(start,10):
				if self.check_value(value, position):
					self.place_value(value, position)
					placed = True
					if self.is_solved():
						self.pretty_print()
						return True
					break
			if placed == False:
				for p in range(i,len(self.to_be_filled_array)):
					self.place_value(0, self.to_be_filled_array[p])
				i = i-1
			else:
				i = i+1


# Goto https://sudoku.game/ and pase the below line to get a sample input and pase the browser console output here
# board = "['";inp = "";for (i = 0; i < 81; i++) {inp += sudoku.getState().board[i].number ? sudoku.getState().board[i].number : 0;if ((i+1)%9 == 0) {board+=inp;inp = "','";}}board += "']";console.log(board)
input = ['010000002','000005490','049700160','000500001','306127904','100009000','061004320','073800000','900000040']
s = Sudoku(input)
s.solve()
