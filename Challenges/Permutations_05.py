
class Permutation(object):
    """docstring for Permutation."""

    def __init__(self, inp):
        self.inp = [];
        for i in inp:
            self.inp.append(str(i))

    def solve(self):
        out = [[self.inp[0]]]
        for i in self.inp[1:]:
            temp = []
            for j in out:
                for k in range(len(j)+1):
                    b = list(j)
                    b.insert(k,i)
                    temp.append(b)
            out = temp;
        return out;

# inp = [43, 20, 45, 38, 31]
# per = Permutation(inp)
# out = per.solve()
# for i in out:
#     print i
# print len(out)
