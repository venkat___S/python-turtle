# To create utility functions that can be commonly used in the challenges

class Utils:

    # Filling an array with default input
    # length -> Length of an Array
    # text_to_be_filled -> the text that will placed in every cell of this array
    @staticmethod
    def fill_array(length, text_to_be_filled):
        return [text_to_be_filled] * (length)

    @staticmethod
    def print_to_file(filename, text):
        f = open(filename, "a+")
        f.write(str(text)+"\n")
        f.close() 
