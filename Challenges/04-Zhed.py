# https://play.google.com/store/apps/details?id=com.groundcontrol.zhed&hl=en_IN
# Implementation, Solving and Generation of the above mentioned android puzzle game
# There are no index references only positions

from Utils import *
from Permutations_05 import *
import sys, time

class zhed(object):
    """docstring for zhed."""

    possible_moves = ["L","R","U","D"]

    # Sample 4x4 Grid representation

    # F F F F
    # F 2 F 1
    # T F F 2
    # F F D T

    # F -> The cell is not filled
    # T -> The cell is filled
    # 2,1 (any integer > 0) -> No of moves you can make in any direction
    # D -> Destination to be reached (Goal)


    # Size -> Size of the puzzle grid (int)
    # movable_positions -> Json contains the position (int) and No.Of moves (int)
    # destination -> To be reached by sliding the puzzle
    def __init__(self, size, movable_positions, destination):
        self.size = size
        self.movable_positions = movable_positions
        self.destination = destination
        self.is_solved = False;
        self.fill_cells()

    def pretty_print(self, board):
        prev = 0;
        count = 1;
        print "----"*self.size
        for i in range(self.size,self.size*self.size+1,self.size):
            print_str = "";
            for j in board[prev:i]:
                count_str = (str(count), "0"+str(count)) [count < 10]
                print_str += "| "+count_str+"-"+str(j)+" ";
                count += 1
            print print_str+"|"
            print "----"*self.size
            prev = i

    def move(self, position, direction, board):
        to_be_filled = self.get_cells(position,direction, board)
        # print to_be_filled, position, direction
        board = self.fill_true(to_be_filled, board);
        return board

    def get_cells(self, position, direction, board):
        i = 0;
        count = board[position-1];
        cell = position - 1;
        output = [cell];
        if direction == "L":
            while i < count:
                cell = cell - 1;
                if ((cell % self.size) == (self.size-1)) or cell < 1 or cell > self.size*self.size:
                    break;
                if board[cell] == "F" or board[cell] == "D":
                    output.append(cell);
                    i += 1;
        elif direction == "R":
            while i < count:
                cell = cell + 1;
                if (cell % self.size) == 1 or cell < 1 or cell > self.size*self.size:
                    break;
                if board[cell] == "F" or board[cell] == "D":
                    output.append(cell);
                    i += 1;
        elif direction == "U":
            while i < count:
                cell = cell - self.size;
                if cell < 1 or cell > self.size*self.size:
                    break;
                if board[cell] == "F" or board[cell] == "D":
                    output.append(cell);
                    i += 1;
        elif direction == "D":
            while i < count:
                cell = cell + self.size;
                if cell < 1 or cell > self.size*self.size:
                    break;
                if board[cell] == "F" or board[cell] == "D":
                    output.append(cell);
                    i += 1;
        for i in range(len(output)):
            output[i] += 1;
        return output;

    def fill_true(self, list, board):
        board_copy = board[:]
        for i in list:
            board_copy[i-1] = "T";
        return board_copy;

    def solve_inner(self, board, movable_positions):
        board_copy = board[:]
        positions_keys = movable_positions[:]
        # Utils.print_to_file("log.txt",movable_positions)
        for i in range(len(positions_keys)):
            position = int(positions_keys[i])
            for j in self.possible_moves:
                new_board = self.move(position,j,board_copy)
                if new_board[self.destination-1] == "T":
                    print "solved"
                    self.pretty_print(new_board)
                    print position, j
                    self.is_solved = True;
                    return True
                elif self.is_solved == False and len(positions_keys[i+1:]) > 0:
                    if self.solve_inner(new_board, positions_keys[i+1:]) == True:
                        self.pretty_print(new_board)
                        print position, j
                        return True;
                    else:
                        continue

    def possible_final_moves(self):
        out = [15, 49];
        return out;

    def solve(self):
        final_moves = self.possible_final_moves()
        for j in final_moves:
            keys = self.movable_positions.keys()
            keys.remove(j)
            perm = Permutation(keys);
            possiblility = perm.solve()
            for i in possiblility:
                print i
                i.append(j)
                if self.solve_inner(self.board[:],i) == True:
                    return True

    def fill_cells(self):
        self.board = Utils.fill_array(self.size*self.size, "F")
        positions = self.movable_positions.keys()
        for position in positions:
            self.board[position-1] = self.movable_positions[position]
        self.board[self.destination-1] = "D"
        self.pretty_print(self.board)


# positions = {35: 2, 45: 2};
positions = {15:2, 34:3, 42:4, 49:2, 67:1, 68:1, 78:1, 86:2}
zhed = zhed(10, positions, 19)
# zhed.solve(zhed.board,zhed.movable_positions.keys())

# positions = {20:1, 31:1, 38:1, 43:2, 45:2}
# zhed = zhed(8, positions, 26)
# zhed.solve(zhed.board,zhed.movable_positions.keys())
zhed.solve()
