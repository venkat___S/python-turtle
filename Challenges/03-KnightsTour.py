
class KnightsTour:

	# Just initialize the class.
	# board_size -> Size of the board 6x6, 8x8
	# path -> Path of knights move in a list.
	# filled_up_cells -> To check whether the board is filled or not.
	# possible_moves_cache -> To maintain the cache of possible_moves
	# May improve some performance :-)
	def __init__(self, board_size=9):
		self.board_size = board_size;
		self.path = [];
		self.filled_up_cells = {}
		self.possible_moves_cache = {}
		self.create_board()

	# To create the 8x8 Chess board.
	# There is no other use for this function.
	# Maybe used for showing after the puzzle is solved.
	def create_board(self):
		self.board = [];
		for i in range(self.board_size,0,-1):
			row = [];
			char_num = 65
			for j in range(self.board_size):
				cell = chr(char_num+j) + str(i)
				row.append(cell)
			self.board.append(row)
		return

	# Listing possible moves from the current position of Knights
	# Used cache kind of thing in "self.possible_moves_cache"
	# This may reduce the loop :-)
	def possible_moves(self, location):
		possible_moves = [];
		try:
			if self.possible_moves_cache[location]:
				return self.possible_moves_cache[location]
		except:
			pass;
		# [(a,b)] -> a - to find the letter (A - H), b - to find the number (1 - 8)
		moves = [(+2,+1),(-2,-1),(+2,-1),(-2,+1), (+1,+2),(-1,-2),(+1,-2),(-1,+2)]
		letter = ord(location[0]);
		number = int(location[1]);
		for i in moves:
			move = "";
			if letter + i[0] >= 65 and ((letter + i[0]) < (65 + self.board_size)):
				move = move + chr(letter + i[0])
				if number + i[1] >= 1 and ((number + i[1]) <= self.board_size):
					move = move + str(number + i[1])
					possible_moves.append(move)
		self.possible_moves_cache[location] = possible_moves;
		return possible_moves;

	# Just checking whether the position is filled or Not
	def is_filled(self, position):
		try:
			return self.filled_up_cells[position];
		except:
			return False

	# Solving the puzzle here using the backtracking algorithm
	# Works for small size board say 6x6, but taking too much
	# Cannot able to solve 8x8, algorithm is good
	# But its taking too much time
	# TODO: Find a way to decrease time, must solve 8x8 puzzle.
	def solve(self, start):
		self.path.append(start)
		self.filled_up_cells[start] = True;
		possible_moves = self.possible_moves(start);
		for possible_move in possible_moves:
			if self.is_filled(possible_move) == False:
				if self.solve(possible_move) == True:
					return True;
				else:
					removed_path = self.path.pop();
					self.filled_up_cells[removed_path] = False;
		if len(self.path) == self.board_size*self.board_size:
			self.pretty_print(self.path)
			return True
		return False

	# Printing the solved moves in the console
	# For viewing and analyzing the output
	def pretty_print(self, data):
		new_board = list(self.board)
		print "-----"*self.board_size
		for i in range(len(new_board)):
			for j in range(len(new_board[i])):
				position = str(data.index(new_board[i][j])+1)
				if len(position) == 1:
					position = "0"+position
				new_board[i][j] = position
			print "|"," | ".join(new_board[i]),"|"
			print "-----"*self.board_size


t1 = KnightsTour(6)
t1.solve("A6")
